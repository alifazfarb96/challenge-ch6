const { user_games, user_game_histories, user_game_bios } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_games.findAll(
                {
                include: [
                        { model: user_game_histories },
                        {model: user_game_bios},
                ]
            }
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_games.create({
                email: req.body.email,
                username: req.body.username,
                password: req.body.password
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user_games.update({
                email: req.body.email,
                username: req.body.username,
                password: req.body.password
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_games.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
