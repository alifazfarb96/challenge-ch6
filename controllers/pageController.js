const path = require('path')

module.exports = {
    home: (req, res) => {
        res.sendFile(path.join(__dirname,"../html/mainpage.html"))
    },
    game: (req, res) => {
        res.sendFile(path.join(__dirname,"../html/game.html"))
    },
    login: (req, res) => {
        const users = {
            "email": "test@dummy.com",
            "password" : "1234"
        }

        if (req.body.email === users.email && req.body.password === users.password) {
            return res.status(200).json({
                "message": "Login berhasil",
                "data": users,
            })
        }

        return res.status(400).json({
            "message": "E-mail atau password salah"
        })
    },
}