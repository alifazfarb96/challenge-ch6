const { user_games, user_game_bios } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game_bios.findAll(
                {
                include: [
                    {model: user_games}
                ]
            }
            );

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_bios.create({
                user_id: req.body.user_id,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                dob: req.body.dob,
                gender: req.body.gender,
            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await user_game_bios.update({
                user_id: req.body.user_id,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                dob: req.body.dob,
                gender: req.body.gender,
            }, {
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_bios.destroy({
                where : {
                    id: req.body.id
                }
            })

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 
