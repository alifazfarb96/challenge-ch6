const express = require('express')
const router = express.Router()
const userController = require('./controllers/userController');
const bioController = require('./controllers/bioController');
const historyController = require('./controllers/historyController');
const pageController = require('./controllers/pageController');

router.get('/api/user/list', userController.list)
router.post('/api/user/create', userController.create)
router.put('/api/user/update', userController.update)
router.delete('/api/user/destroy', userController.destroy)

router.get('/api/bio/list', bioController.list)
router.post('/api/bio/create', bioController.create)
router.put('/api/bio/update', bioController.update)
router.delete('/api/bio/destroy', bioController.destroy)

router.get('/api/history/list', historyController.list)
router.post('/api/history/create', historyController.create)
router.put('/api/history/update', historyController.update)
router.delete('/api/history/destroy', historyController.destroy)

router.get('/', pageController.home);
router.get('/game', pageController.game);
router.post('/login', pageController.login);

module.exports = router
